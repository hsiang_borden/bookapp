const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');

const app = express();

const redis = require('redis');
let RedisStore = require('connect-redis')(session);
let redisClient = redis.createClient();
const SESS_LIFETIME = 1000 * 10; //60 * 60 * 12; // 12 HOURS
app.use(
	session({
		store: new RedisStore({ client: redisClient }),
		name: process.env.SESS_NAME,
		resave: false,
		saveUninitialized: false,
		secret: process.env.SESS_SECRET,
		cookie: {
			maxAge: SESS_LIFETIME,
			sameSite: true,
			secure: process.env.CURRENT_ENV === 'production'
		}
	})
);

app.use(flash());

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(methodOverride('__method'));

app.use((req, res, next) => {
	const err = req.flash('error');
	const success = req.flash('success');
	res.locals.error = err.length ? err : null;
	res.locals.success = success.length ? success : null;
	res.locals.currentUser = req.session.user;
	next();
});

app.use(require('./router/api/authorRoutes'));
app.use(require('./router/api/bookRoutes'));
app.use(require('./router/api/authRoutes'));

app.use(require('./router/authorRoutes'));
app.use(require('./router/bookRoutes'));
app.use(require('./router/authRoutes'));

if ((process.env.CURRENT_ENV = 'development')) {
	app.use(require('./router/testRoutes'));
}

app.get('/', (req, res) => {
	res.render('index');
});

module.exports = app;
