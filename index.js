const app = require('./app');

app.listen(process.env.PORT, () => {
	console.log(`App start! PORT=${process.env.PORT}`);
});
