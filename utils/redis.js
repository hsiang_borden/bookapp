const redis = require('redis'),
	client = redis.createClient({
		db: process.env.REDIS_DB
	});

client.on('error', function(err) {
	console.log('Error ' + err);
});

// client.on('connect', function(err) {
// 	console.log('Redis connected.');
// });

client.setObjs = (keyName, objs, expireTime = 0) => {
	client.set(keyName + ':count', objs.length);
	if (expireTime) {
		client.expire(keyName + ':count', expireTime);
	}
	objs.forEach((element, index) => {
		const key = `${keyName}:${index}`;
		client.hmset(key, element);
		if (expireTime) {
			client.expire(key, expireTime);
		}
	});
};

function hgetall(client, keyName) {
	return new Promise((resolve, reject) => {
		client.hgetall(keyName, function(err, obj) {
			if (err) {
				return reject(err);
			}
			return resolve(obj);
		});
	});
}

client.getObjs = keyName => {
	return new Promise((resolve, reject) => {
		client.get(keyName + ':count', async (err, reply) => {
			if (err) {
				return reject(err);
			}
			if (!reply) {
				return resolve(null);
			}
			let results = [];
			for (let i = 0; i < reply; i++) {
				const element = await hgetall(client, `${keyName}:${i}`);
				if (!element) {
					resolve(null);
				}
				results.push(element);
			}
			resolve(results);
		});
	});
};

client.delObjs = keyName => {
	return new Promise((resolve, reject) => {
		client.get(keyName + ':count', async (err, reply) => {
			if (err) {
				return reject(err);
			}
			if (!reply) {
				return resolve();
			}
			let keys = [keyName + ':count'];
			for (let i = 0; i < reply; i++) {
				keys.push(`${keyName}:${i}`);
			}
			console.log(keys);
			client.del(keys, err => {
				if (err) {
					reject(err);
				}
				resolve();
			});
		});
	});
};

module.exports = client;
