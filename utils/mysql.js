const mysql = require('mysql');
const bluebird = require('bluebird');

const connect = mysql.createConnection({
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_NAME
});
connect.queryAsync = bluebird.promisify(connect.query);

module.exports = connect;
