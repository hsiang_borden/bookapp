const router = require('express').Router();
const user = require('../../model/user');

router.post('/api/login', async (req, res) => {
	try {
		const result = await user.authenticate({
			account: req.body.account,
			password: req.body.password
		});
		req.session.user = result;
		res.status(200).send({ status: 'success' });
	} catch (error) {
		res.send(error);
	}
});

router.post('/api/logout', (req, res) => {
	req.session.user = null;
	res.status(200).send({ status: 'success' });
});

router.post('/api/register', async (req, res) => {
	try {
		const result = await user.create({
			account: req.body.account,
			password: req.body.password,
			name: req.body.name,
			email: req.body.email
		});
		res.send(result);
	} catch (error) {
		res.send(error);
	}
});

router.get('/api/user', (req, res) => {
	res.status(200).send(req.session.user);
});

module.exports = router;
