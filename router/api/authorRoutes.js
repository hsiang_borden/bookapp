const router = require('express').Router();
const author = require('../../model/author');

router.post('/api/authors/get', (req, res) => {
	author.list((err, results) => {
		if (err) {
			return res.status(400).send('Something went wrong');
		}
		res.status(200).send(results);
	});
});

router.post('/api/authors/post', async (req, res) => {
	try {
		await author.create(req.body.name, req.body.age);
		res.status(201).send({ status: 'success' });
	} catch (err) {
		res.status(400).send();
	}
});

router.post('/api/authors/delete', (req, res) => {
	author.remove(req.body.id, err => {
		if (err) {
			return res.status(400).send();
		}
		res.status(200).send({ status: 'success' });
	});
});

router.post('/api/authors/get/ById', async (req, res) => {
	try {
		const result = await author.searchById(req.body.id);
		res.status(200).send(result);
	} catch (e) {
		res.status(400).send(e);
	}
});

router.post('/api/authors/get/ByName', async (req, res) => {
	try {
		const result = await author.searchByName(req.body.name);
		res.status(200).send(result);
	} catch (e) {
		res.status(400).send(e);
	}
});

router.post('/api/authors/put', async (req, res) => {
	try {
		const result = await author.update(req.body);
		res.status(200).send({ status: 'success' });
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;
