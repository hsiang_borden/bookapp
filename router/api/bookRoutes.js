const router = require('express').Router();
const book = require('../../model/book');

router.post('/api/books/get', (req, res) => {
	book.list((err, results) => {
		if (err) {
			return res.status(400).send('Something went wrong');
		}
		res.status(200).send(results);
	});
});

router.post('/api/books/post', async (req, res) => {
	try {
		await book.create(req.body.name, req.body.authorId);
		res.status(201).send({ status: 'success' });
	} catch (error) {
		return res.status(400).send();
	}
});

router.post('/api/books/delete', (req, res) => {
	book.remove(req.body.id, err => {
		if (err) {
			return res.status(400).send();
		}
		res.status(200).send({ status: 'success' });
	});
});

router.post('/api/books/get/ById', async (req, res) => {
	try {
		const result = await book.searchById(req.body.id);
		res.status(200).send(result);
	} catch (e) {
		console.log(e);
		res.status(400).send(e);
	}
});

router.post('/api/books/get/ByName', async (req, res) => {
	try {
		const result = await book.searchByName(req.body.name);
		res.status(200).send(result);
	} catch (e) {
		console.log(e);
		res.status(400).send(e);
	}
});

router.post('/api/books/get/ByAuthorId', async (req, res) => {
	try {
		const result = await book.searchByAuthorId(req.body.authorId);
		res.status(200).send(result);
	} catch (e) {
		console.log(e);
		res.status(400).send(e);
	}
});

router.post('/api/books/put', async (req, res) => {
	try {
		const result = await book.update(req.body);
		res.status(200).send({ status: 'success' });
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;
