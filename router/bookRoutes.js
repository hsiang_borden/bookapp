const router = require('express').Router();
const book = require('../model/book');

router.get('/books', (req, res) => {
	book.list((err, results) => {
		if (err) {
			return res.status(400).send('Something went wrong');
		}
		res.render('books', { books: results });
	});
});

router.post('/books', async (req, res) => {
	try {
		await book.create(req.body.name, Number(req.body.authorId));
		req.flash('success', 'Successfully created a book.');
	} catch (error) {
		req.flash('error', error);
	}
	res.redirect('/books');
});

router.delete('/books/:id', (req, res) => {
	book.remove(req.params.id, err => {
		if (err) {
			req.flash('error', 'Something went wrong');
		} else {
			req.flash('success', 'Successfully delete a book.');
		}
		res.redirect('/books');
	});
});

router.put('/books/:id', async (req, res) => {
	try {
		const result = await book.update({ id: req.params.id, ...req.body });
		req.flash('success', 'Successfully update.');
	} catch (error) {
		req.flash('error', 'AuthorID is not valid.');
	}
	res.redirect('/books');
});

module.exports = router;
