const router = require('express').Router();
const author = require('../model/author');

router.get('/authors', (req, res) => {
	author.list((err, results) => {
		if (err) {
			return res.status(400).send('Something went wrong');
		}
		res.render('authors', { authors: results });
	});
});

router.post('/authors', async (req, res) => {
	try {
		await author.create(req.body.name, Number(req.body.age));
		req.flash('success', 'Sucessfully created an author.');
	} catch (err) {
		req.flash('error', err);
	}
	res.redirect('/authors');
});

router.delete('/authors/:id', (req, res) => {
	author.remove(req.params.id, err => {
		if (err) {
			req.flash('error',"can't not delete author with a book in database.");
		} else {
			req.flash('success','Successfully delete author.')
		}
		res.redirect('/authors');
	});
});

router.put('/authors/:id', async (req, res) => {
	try {
		const result = await author.update({ id: req.params.id, ...req.body });
		req.flash('success', 'Successfully update author.');
	} catch (error) {
			req.flash(
				'error',
				"update error."
			);
	}
		res.redirect('/authors');
});

module.exports = router;
