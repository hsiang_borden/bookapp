const router = require('express').Router();
const user = require('../model/user');

router.get('/test1', async (req, res) => {
	try {
		const result = await user.create({
			account: 'a179346',
			password: 'password',
			name: 'hsiang',
			email: 'hsiang@borden.com.tw'
		});
		res.send(result);
	} catch (error) {
		res.send(error);
	}
});

router.get('/test2', async (req, res) => {
	try {
		const result = await user.authenticate({
			account: 'a179346',
			password: 'password2'
		});
		res.send(result);
	} catch (error) {
		res.send(error);
	}
});

module.exports = router;
