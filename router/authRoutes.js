const router = require('express').Router();
const user = require('../model/user');

router.get('/login', (req, res) => {
  res.render('login');
})

router.post('/login', async (req, res) => {
  try {
    console.log(req.body);
    const result = await user.authenticate({
      account: req.body.account,
      password: req.body.password
    });
    console.log('result:', result);
    req.session.user = result;
    req.flash('success',`Welcome back! ${result.name}`)
  } catch (error) {
    req.flash('error',error)
  }
    res.redirect('/');
});

router.get('/logout', (req, res) => {
  req.session.user = null;
  req.flash('success','Successfully logout.')
  res.redirect('/');
});

router.get('/register', (req, res) => {
  res.render('register');
});

router.post('/register', async (req, res) => {
  try {
    if (req.body.password !== req.body.confirmPassword) {
      return res.redirect('/register');
    }
    const result = await user.create({
      account: req.body.account,
      password: req.body.password,
      name: req.body.name,
      email: req.body.email
    });
    req.flash('success','Successfully registered.')
    res.redirect('/login');
  } catch (error) {
    req.flash('error',error)
    res.redirect('/register');
  }
});

module.exports = router;
