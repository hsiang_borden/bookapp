const assert = require('assert');
const supertest = require('supertest');
const app = require('../../app');
const request = supertest(app);

const books = [{ name: 'book0' }, { name: 'book1' }, { name: 'book2' }];
const updatedBook1Name = 'book4';
let authors;

describe('Book Routes', () => {
	before(async () => {
		let res = await request.post('/api/authors/get').send();
		authors = res.body;
		books[0].authorId = authors[0].id;
		books[1].authorId = authors[1].id;
		books[2].authorId = authors[1].id;
		res = await request
			.post('/api/books/post')
			.send({ name: books[0].name, authorId: books[0].authorId })
			.expect('Content-type', /json/)
			.expect(201);
		assert.deepEqual(res.body, { status: 'success' });

		res = await request
			.post('/api/books/post')
			.send({ name: books[1].name, authorId: books[1].authorId })
			.expect('Content-type', /json/)
			.expect(201);
		assert.deepEqual(res.body, { status: 'success' });
	});

	it('Create Book: should create book2', async () => {
		const { body } = await request
			.post('/api/books/post')
			.send({ name: books[2].name, authorId: books[2].authorId })
			.expect('Content-type', /json/)
			.expect(201);
		assert.deepEqual(body, { status: 'success' });
	});

	it('Create Book: should not create book with invalid author', async () => {
		const { body } = await request
			.post('/api/books/post')
			.send({ name: books[2].name, authorId: books[2].authorId + 200 })
			.expect(400);
	});

	it('Create Book: should not create book without name or authorId', async () => {
		await request
			.post('/api/books/post')
			.send({ authorId: books[2].authorId })
			.expect(400);

		await request
			.post('/api/books/post')
			.send({ name: 'valid book name' })
			.expect(400);
	});

	it('Get Books: should return three books match book0,book1,book2', async () => {
		let { body } = await request.post('/api/books/get').send();
		assert.equal(body.length, 3);
		const booksName = books.map(b => b.name);
		const booksAuthorId = books.map(b => b.authorId);
		body.forEach(book => {
			assert('id' in book);
			const nameIdx = booksName.indexOf(book.name);
			assert.notEqual(nameIdx, -1);
			assert(booksAuthorId.includes(book.authorId));
			books[nameIdx].id = book.id;
		});
	});

	it('Delete Book: should delete book0', async () => {
		let { body } = await request
			.post('/api/books/delete')
			.send({ id: books[0].id })
			.expect('Content-type', /json/)
			.expect(200);
		assert.deepEqual(body, { status: 'success' });
	});

	it('Get Book By Id: should get book1', async () => {
		let { body } = await request
			.post('/api/books/get/ById')
			.send({ id: books[1].id })
			.expect('Content-type', /json/)
			.expect(200);
		assert.deepEqual(body, { author: authors[1].name, ...books[1] });
	});

	it('Get Book By Id: should not get book0 which is deleted', async () => {
		await request
			.post('/api/books/get/ById')
			.send({ id: books[0].id })
			.expect(400);
	});

	it('Get Book By Name: should get book1', async () => {
		let { body } = await request
			.post('/api/books/get/ByName')
			.send({ name: books[1].name })
			.expect('Content-type', /json/)
			.expect(200);
		assert.deepEqual(body, { author: authors[1].name, ...books[1] });
	});

	it('Update Book: should update book1', async () => {
		books[1].name = updatedBook1Name;
		const res = await request
			.post('/api/books/put')
			.send(books[1])
			.expect('Content-type', /json/)
			.expect(200);
		assert.deepEqual(res.body, { status: 'success' });
	});

	it('Get Book By Author ID: should get book1,book2', async () => {
		let { body } = await request
			.post('/api/books/get/ByAuthorId')
			.send({ authorId: authors[1].id })
			.expect('Content-type', /json/)
			.expect(200);
		assert.deepEqual(body, [
			{ author: authors[1].name, ...books[1] },
			{ author: authors[1].name, ...books[2] }
		]);
	});
});
