const assert = require('assert');
const supertest = require('supertest');
const app = require('../../app');
const request = supertest(app);

let user = {
	account: 'account',
	password: 'account_password',
	name: 'account_name',
	email: 'account@example.com'
};
let cookie;

describe('Auth Router', () => {
	it('Register a user', async () => {
		const res = await request
			.post('/api/register')
			.send(user)
			.expect('Content-type', /json/)
			.expect(200);
		assert.deepEqual(res.body, { status: 'success' });
	});

	it('Login User: should login and get a cookie', async () => {
		const res = await request
			.post('/api/login')
			.send({ account: user.account, password: user.password })
			.expect('Content-type', /json/)
			.expect(200);
		assert.deepEqual(res.body, { status: 'success' });
		cookie = res.headers['set-cookie'].pop().split(';')[0];
		console.log(cookie);
	});

	it('Get User Info: should get user info after login successfully', async () => {
		const req = request.get('/api/user');
		req.cookies = cookie;

		const { body } = await req
			.set('Accept', 'application/json')
			.expect('Content-Type', /json/)
			.expect(200);

		assert('created' in body);
		delete body.created;
		assert.deepEqual(body, {
			account: user.account,
			name: user.name,
			email: user.email,
			isAdmin: 0
		});
	});

	it('Logout: should logout', async () => {
		const req = request.post('/api/logout');
		req.cookies = cookie;
		const { body } = await req.send().expect(200);

		assert.deepEqual(body, { status: 'success' });
	});

	it('Get User Info: should not get user info after logout', async () => {
		const req = request.get('/api/user');
		req.cookies = cookie;

		const { body } = await req.send().expect(200);

		assert.deepEqual(body, {});
	});
});
