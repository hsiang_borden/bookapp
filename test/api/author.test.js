const assert = require('assert');
const supertest = require('supertest');
const app = require('../../app');
const request = supertest(app);
const mysql = require('../../utils/mysql');

const authors = [
	{ name: 'test1', age: 11 },
	{ name: 'test2', age: 12 },
	{ name: 'test3', age: 13 }
];

author2_updated = { name: 'test4', age: 14 };

describe('API TEST', () => {
	before(async () => {
		const delBook = await mysql.queryAsync('DELETE FROM book');
		const delAuthor = await mysql.queryAsync('DELETE FROM author');
		const delUser = await mysql.queryAsync('DELETE FROM USER');
	});

	describe('Author Routes', function() {
		before(async () => {
			const res1 = await request
				.post('/api/authors/post')
				.send(authors[0])
				.expect('Content-Type', /json/)
				.expect(201);
			assert.deepEqual(res1.body, { status: 'success' });

			const res2 = await request
				.post('/api/authors/post')
				.send(authors[1])
				.expect('Content-Type', /json/)
				.expect(201);
			assert.deepEqual(res2.body, { status: 'success' });
		});

		it('should create author3', async () => {
			const res = await request
				.post('/api/authors/post')
				.send(authors[2])
				.expect('Content-Type', /json/)
				.expect(201);
			assert.deepEqual(res.body, { status: 'success' });
		});

		it('should not create author without name', async () => {
			const res = await request
				.post('/api/authors/post')
				.send({ age: 15 })
				.expect(400);
		});

		it('should not create author with same name', async () => {
			const res = await request
				.post('/api/authors/post')
				.send({ name: authors[0].name, age: 25 })
				.expect(400);
		});

		it('should get 3 authors and match author1,author2,author3', async () => {
			const res = await request
				.post('/api/authors/get')
				.send()
				.expect(200);

			assert(res.body.length === 3);
			const authorsName = authors.map(a => a.name);
			const authorsAge = authors.map(a => a.age);
			res.body.forEach((author, i) => {
				assert('id' in author);
				const nameIdx = authorsName.indexOf(author.name);
				const ageIdx = authorsAge.indexOf(author.age);
				assert.notEqual(nameIdx, -1);
				assert.notEqual(ageIdx, -1);
				authors[nameIdx].id = author.id;
			});
		});

		it('should delete author1', async () => {
			const res = await request
				.post('/api/authors/delete')
				.send({ id: authors[0].id })
				.expect('Content-Type', /json/)
				.expect(200);
			assert.deepEqual(res.body, { status: 'success' });
		});

		it('when delete should return status code 400 when id is not specified', async () => {
			await request
				.post('/api/authors/delete')
				.send()
				.expect(400);
		});

		it('should get 2 authors match author2,author3', async () => {
			const res = await request
				.post('/api/authors/get')
				.send()
				.expect(200);

			assert(res.body.length === 2);
			res.body.forEach((author, i) => {
				assert('id' in author);
				assert.notEqual(author.id, authors[0].id);
				assert.notEqual(author.name, authors[0].name);
				assert.notEqual(author.age, authors[0].age);
			});
		});

		it('should return author2 when get by author2.id', async () => {
			const res = await request
				.post('/api/authors/get/ById')
				.send({ id: authors[1].id })
				.expect(200)
				.expect('Content-Type', /json/);
			assert.deepEqual(res.body, authors[1]);
		});

		it('should update author2', async () => {
			authors[1].name = author2_updated.name;
			authors[1].age = author2_updated.age;
			const res = await request
				.post('/api/authors/put')
				.send(authors[1])
				.expect(200);
			assert.deepEqual(res.body, { status: 'success' });
		});

		it('get author by name: should return author3', async () => {
			const res = await request
				.post('/api/authors/get/ByName')
				.send({ name: authors[2].name })
				.expect('Content-type', /json/)
				.expect(200);
			assert.deepEqual(res.body, authors[2]);
		});
	});

	it('should equal', () => {
		assert.equal(1, 1);
	});
});
