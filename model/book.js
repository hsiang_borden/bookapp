const mysql = require('../utils/mysql');
const redis = require('../utils/redis');
const REDIS_BOOK_KEY = 'books';

mysql.query(
	`CREATE TABLE IF NOT EXISTS book (
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(50) NOT NULL,
author_id INT NOT NULL,
PRIMARY KEY(ID),
FOREIGN KEY (author_id) REFERENCES author(id)
)`,
	(error, results, fields) => {
		if (error) throw error;
	}
);

const create = async (name, authorId) => {
	if (!name || !authorId)
		return Promise.reject('Please fill in name & author id');
	if (typeof name !== 'string') return Promise.reject('name is not a string');
	if (typeof authorId !== 'number')
		return Promise.reject('author id is not a number');
	try {
		await mysql.queryAsync(
			`INSERT INTO book (name,author_id) VALUES ("${name}",${authorId})`
		);
		await redis.delObjs(REDIS_BOOK_KEY);
		return { status: 'success' };
	} catch (error) {
		return Promise.reject('invalid author id');
	}
};

const list = async callback => {
	let books = await redis.getObjs(REDIS_BOOK_KEY);
	if (books) {
		return callback(null, books);
	}
	mysql.query(
		`SELECT book.id,book.name,book.author_id AS authorId,author.NAME AS author FROM book
  INNER JOIN author ON book.author_id=author.id`,
		(error, results, fields) => {
			if (error) {
				return callback(error);
			}
			redis.setObjs(REDIS_BOOK_KEY, results);
			return callback(null, results);
		}
	);
};

const remove = (id, callback) => {
	mysql.query(
		`DELETE FROM book WHERE id=${id}`,
		async (error, results, fields) => {
			if (error) {
				console.log('delete book fail');
			}
			await redis.delObjs(REDIS_BOOK_KEY);
			callback(error);
		}
	);
};

const searchById = id => {
	return new Promise((resolve, reject) => {
		if (!id) {
			return reject('Please Enter Id.');
		}

		mysql.query(
			`SELECT book.id,book.name,book.author_id AS authorId,author.NAME AS author FROM book
  INNER JOIN author ON book.author_id=author.id WHERE book.id=?`,
			[id],
			(error, results, fields) => {
				if (error) {
					return reject(error);
				}
				if (!results[0]) return reject('Book not found');
				resolve(results[0]);
			}
		);
	});
};

const searchByName = name => {
	return new Promise((resolve, reject) => {
		if (!name) {
			return reject('Please Enter name.');
		}

		mysql.query(
			`SELECT book.id,book.name,book.author_id AS authorId,author.NAME AS author FROM book
  INNER JOIN author ON book.author_id=author.id WHERE book.name=?`,
			[name],
			(error, results, fields) => {
				if (error) {
					return reject(error);
				}
				if (!results[0]) return reject('Book not found');
				resolve(results[0]);
			}
		);
	});
};

const searchByAuthorId = authorId => {
	return new Promise((resolve, reject) => {
		if (!authorId) {
			return reject("Please Enter Author's Id.");
		}

		mysql.query(
			`SELECT book.id,book.name,book.author_id AS authorId,author.NAME AS author FROM book
  INNER JOIN author ON book.author_id=author.id WHERE author_id=?`,
			[authorId],
			(error, results, fields) => {
				if (error) {
					return reject(error);
				}
				resolve(results);
			}
		);
	});
};

const update = ({ id, name, authorId }) => {
	return new Promise((resolve, reject) => {
		if (!id) {
			return reject('Please Enter id');
		}
		if (!name || !authorId) {
			return reject("Please Enter Both Book's Name & Author's Id");
		}

		mysql.query(
			'UPDATE book SET name=?,author_id=? WHERE id=?',
			[name, authorId, id],
			async (error, results, fields) => {
				if (error) {
					return reject(error);
				}
				await redis.delObjs(REDIS_BOOK_KEY);
				resolve('Update Success!');
			}
		);
	});
};

module.exports = {
	create,
	list,
	remove,
	searchById,
	searchByName,
	searchByAuthorId,
	update
};
