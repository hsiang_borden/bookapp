const mysql = require('../utils/mysql');
const validator = require('validator');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.CRYPTR_SECRET);

// const encryptedString = cryptr.encrypt('bacon');
// const decryptedString = cryptr.decrypt(encryptedString);

mysql.query(
	`CREATE TABLE IF NOT EXISTS USER (
	account VARCHAR(50),
	password VARCHAR(400) NOT NULL,
	name VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  isAdmin BOOLEAN DEFAULT FALSE,
	PRIMARY KEY(account)
)`,
	(error, results, fields) => {
		if (error) throw error;
	}
);

const create = ({ account, password, name, email }) => {
	return new Promise((resolve, reject) => {
		if (!account || !password || !name || !email) {
			return reject('Please check all information filled.');
		}
		if (!validator.isEmail(email)) {
			return reject('Email is not valid.');
		}
		const encryptedPassword = cryptr.encrypt(password);
		mysql.query(
			`INSERT INTO USER (account,password,name,email) VALUES (?, ?, ?, ?)`,
			[account, encryptedPassword, name, email],
			(error, results, fields) => {
				if (error) {
					return reject(error);
				}
				return resolve({ status: 'success' });
			}
		);
	});
};

const authenticate = ({ account, password }) => {
	return new Promise((resolve, reject) => {
		if (!account || !password) {
			return reject('Please fill both account and password.');
		}
		mysql.query(
			`SELECT * FROM USER WHERE account=?`,
			[account],
			(err, results) => {
				if (err) {
					return reject(err);
				}
				if (!results) {
					return reject('Invalid account.');
				}
				let user = results[0];
				const decryptedpassword = cryptr.decrypt(user.password);
				if (password !== decryptedpassword) {
					return reject('Password is incorrect.');
				}
				delete user.password;
				resolve(user);
			}
		);
	});
};

module.exports = { create, authenticate };
