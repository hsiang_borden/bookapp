const mysql = require('../utils/mysql');
const redis = require('../utils/redis');
const REDIS_AUTHOR_KEY = 'authors';

mysql.query(
	`CREATE TABLE IF NOT EXISTS author (
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(50) NOT NULL UNIQUE,
age INT,
PRIMARY KEY(ID)
)`,
	(error, results, fields) => {
		if (error) throw error;
	}
);

const create = (name, age) => {
	return new Promise(async (resolve, reject) => {
		if (!name) return reject("can't create author without name");
		if (typeof name !== 'string')
			return reject('author name should be a string.');
		if (age && typeof age !== 'number')
			return reject('age should be number');

		let sql = age
			? `INSERT INTO author (name,age) VALUES ("${name}",${age})`
			: `INSERT INTO author (name) VALUES ("${name}")`;
		try {
			res = await mysql.queryAsync(sql);
			await redis.delObjs(REDIS_AUTHOR_KEY);
			return resolve();
		} catch (err) {
			return reject('Author name has been taken');
		}
	});
};

const list = async callback => {
	let authors = await redis.getObjs(REDIS_AUTHOR_KEY);
	if (authors) {
		return callback(null, authors);
	}
	mysql.query('SELECT * FROM author', (error, results, fields) => {
		if (error) {
			return callback(error);
		}

		redis.setObjs(REDIS_AUTHOR_KEY, results);
		return callback(null, results);
	});
};

const remove = (id, callback) => {
	mysql.query(
		`DELETE FROM author WHERE id=${id}`,
		async (error, results, fields) => {
			if (error) {
				console.log('delete author fail');
			}
			await redis.delObjs(REDIS_AUTHOR_KEY);
			callback(error);
		}
	);
};

const searchById = id => {
	return new Promise((resolve, reject) => {
		if (!id) {
			return reject('Please Enter Id.');
		}

		mysql.query(
			'SELECT * FROM author WHERE id=?',
			[id],
			(error, results, fields) => {
				if (error) {
					return reject(error);
				}
				resolve(results[0]);
			}
		);
	});
};

const searchByName = name => {
	return new Promise((resolve, reject) => {
		if (!name) {
			return reject('Please Enter name.');
		}

		mysql.query(
			'SELECT * FROM author WHERE name=?',
			[name],
			(error, results, fields) => {
				if (error) {
					return reject(error);
				}
				resolve(results[0]);
			}
		);
	});
};

const update = ({ id, name, age }) => {
	return new Promise((resolve, reject) => {
		if (!id) {
			return reject('Please Enter id');
		}
		if (!name || !age) {
			return reject("Please Enter Both Author's Name & Age");
		}

		mysql.query(
			'UPDATE author SET name=?,age=? WHERE id=?',
			[name, age, id],
			async (error, results, fields) => {
				if (error) {
					return reject(error);
				}
				await redis.delObjs(REDIS_AUTHOR_KEY);
				resolve('Update Success!');
			}
		);
	});
};

module.exports = { create, list, remove, searchById, searchByName, update };
